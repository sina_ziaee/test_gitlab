package com.example.multitab;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class SimpleTabActivity extends AppCompatActivity {

    TabLayout tabs;
    ViewPager viewPager;

    SlidePagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_tab);

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        adapter = new SlidePagerAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);

        prepareSlides();

        tabs = findViewById(R.id.tabLayout);

        Intent intent = getIntent();
        String which = intent.getStringExtra("mode");

        tabs.setupWithViewPager(viewPager);

//اگر قبل این که آداپتر رو صدا بزنی از متد های setIcon استفاده کنی کارت ‌اره چون هنوز نساحته فرگمنت ها رو
        if (which.equals("simple")){
            tabs.setTabMode(TabLayout.MODE_FIXED);
        }
        else if (which.equals("icon")){
            tabs.getTabAt(0).setIcon(R.drawable.ic);
            tabs.getTabAt(1).setIcon(R.drawable.ic);
            tabs.getTabAt(2).setIcon(R.drawable.ic);
            tabs.getTabAt(3).setIcon(R.drawable.ic);

        }
        else{
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

    }

    private void prepareSlides() {

        ArrayList<String> list = new ArrayList<>();
        list.add("sina");
        list.add("mina");
        list.add("hana");
        list.add("shana");
        list.add("nana");

        String[] titles = getResources().getStringArray(R.array.titles);
        String[] descriptions = getResources().getStringArray(R.array.descriptions);
        int[] backGrounds = {R.color.bgColor1, R.color.bgColor2, R.color.bgColor3, R.color.bgColor4};
        int[] imgResources = {R.drawable.number, R.drawable.family, R.drawable.flag, R.drawable.food};

        for (int i=0;i<backGrounds.length;i++){
            adapter.addFragment(SlideFragment.newInstance(titles[i], descriptions[i],
                    imgResources[i], ContextCompat.getColor(this, backGrounds[i]), list), titles[i]);
        }

    }

    public class SlidePagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragments;
        List<String> titles;

        public SlidePagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<>();
            titles = new ArrayList<>();
        }

        public void addFragment(Fragment fragment, String title){
            fragments.add(fragment);
            titles.add(title);
            //باید pagerAdapter بفهمه که محتوای adapter عوض شده
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }



    }

}
