package com.example.multitab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

public class SlideFragment extends Fragment {

    String title;
    String description;
    int imgRes;
    int bgColor;

    ArrayList<String> list;

    public static SlideFragment newInstance(String title, String description, int img_id, int bgColor, ArrayList<String> list_str) {

        SlideFragment fragment = new SlideFragment();

        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("desc", description);
        bundle.putInt("id", img_id);
        bundle.putInt("bgColor", bgColor);
        bundle.putStringArrayList("list", list_str);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle == null)
            return;

        list = new ArrayList<>();

        title = bundle.getString("title");
        description = bundle.getString("desc");
        imgRes = bundle.getInt("id");
        bgColor = bundle.getInt("bgColor");

        ArrayList<String> temp = bundle.getStringArrayList("list");
        list.addAll(temp);

    }

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.slide, container, false);
        TextView tv_title = view.findViewById(R.id.tv_title);
        TextView tv_description = view.findViewById(R.id.tv_description);
        ImageView imageView = view.findViewById(R.id.img_profile);
        ListView listView = view.findViewById(R.id.list_view);

        tv_title.setText(title);
        tv_description.setText(description);
        imageView.setImageResource(imgRes);

        view.findViewById(R.id.parentLayout).setBackgroundColor(bgColor);

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);

        listView.setAdapter(adapter);
        return view;

    }
}
